<?php
if (isset($_POST["action"])){
	if ($_POST["action"]=="search"){
		$search=htmlspecialchars($_POST["search"]);
		if ($search==""){
			$search="moon";
		}
		$response = file_get_contents('https://api.flickr.com/services/rest/?method=flickr.photos.search&text='.$search.'&format=json&api_key=57f694132e4714c29a64c9af890b124e');
		$response = str_replace( 'jsonFlickrApi(', '', $response );
		$response = substr( $response, 0, strlen( $response ) - 1 ); 
		$response = json_decode($response, TRUE);
		print json_encode($response["photos"]["photo"]);
	}
	else if ($_POST["action"]=="add"){
		$parent=$_POST["parent"];
		$name=$_POST["name"];
		$keys = file_get_contents("keywords.json");
		if(strpos($keys,'"'.$name.'"')==false){		
			$keys = json_decode($keys, TRUE);
			add($keys,$parent,$name);
			file_put_contents('keywords.json', json_encode($keys));
		}
	}
	else if ($_POST["action"]=="delete"){
		$name=$_POST["name"];
		$keys = file_get_contents("keywords.json");
		$keys = json_decode($keys, TRUE);
		delete($keys,$name);
		file_put_contents('keywords.json', json_encode($keys));
	}
}
function add(&$keys,$parent,$name){
	if($parent==""){
		$obj["name"]=$name;
		$obj["children"]=Array();
		$keys[]=$obj;
	}
	else if(count($keys)>0){
		$finish=false;
		for ($i=0;$i<count($keys);$i++){
			if ($keys[$i]["name"]==$parent){
				$obj["name"]=$name;
				$obj["children"]= Array();
				$keys[$i]["children"][]=$obj;
				$finish=true;
			}
		}
		if ($finish==false){
			for ($i=0;$i<count($keys);$i++){
				add($keys[$i]["children"],$parent,$name);
			}
		}
	}
}
function delete(&$keys,$name){
	if(count($keys)>0){
		$finish=false;
		for ($i=0;$i<count($keys);$i++){
			if ($keys[$i]["name"]==$name){
				array_splice($keys, $i, 1);
				$finish=true;
			}
		}
		if ($finish==false){
			for ($i=0;$i<count($keys);$i++){
				delete($keys[$i]["children"],$name);
			}
		}
	}
}
?>