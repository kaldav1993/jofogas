<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Flickr Image Search</title>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<script src="js/modernizr.custom.js"></script>
	<script type="text/javascript" src="jquery-1.12.0.min.js"></script>
</head>
<body>
<div class="container">
<div style="width:100%;text-align:center;">
	<form id="search_form" class="ajax_submit">
		Kulcsszó: <input type="text" id="key" name="search">
		<button type="submit"> Keresés</button>
		<input type="hidden" name="action" value="search">
	</form>
	<br>Kulcsszavak<br>
	<div style="display:inline-block;text-align:left;">
	<?php
		include "tree.php";
	?>
	</div>
</div>
<div id="grid-gallery" class="grid-gallery">
	<section class="grid-wrap">
		<ul class="grid" id="result_div">
		</ul>
	</section>
</div>
</div>
<div id="hidden" style="display:none">
	<li class="img_cell">
		<figure>
			<img src=""/>
			<figcaption><p class="title"></p></figcaption>
		</figure>
	</li>
</div>

</body>
<script>
	$(document).on('submit','.ajax_submit',function(){
		 $.ajax({
           type: "POST",
           url: "api.php",
           data: $(this).serialize(),
           success: function(data)
           {
				$("#result_div").html("");
				var result = JSON.parse(data);
				for (i=0; i<result.length;i++){
					element=$("#hidden").find(".img_cell").clone();
					element.find(".title").html(result[i].title);
					element.find("img").attr("src","https://farm"+result[i].farm+".staticflickr.com/"+result[i].server+"/"+result[i].id+"_"+result[i].secret+".jpg");
					element.appendTo("#result_div");
				}
           }
         });
		return false;
	});
	$(".keyword").click(function(){
		$("#key").val($(this).data("name"));
		$("#search_form").submit();
	});
	$(".add, .delete").click(function(){
		$.ajax({
           type: "POST",
           url: "api.php",
           data: $(this).closest("form").serialize(),
           success: function(data){
				window.location.reload();
           }
         });
	});
</script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<script>
	new CBPGridGallery( document.getElementById( 'grid-gallery' ) );
</script>
</html>